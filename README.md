# GoRestApi
![GoRestApiGo](https://syngress.pl/images/go_rest_api/go.png) ![GoRestApiKafka](https://syngress.pl/images/go_rest_api/kafka.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Things you may want to cover for local development:

* Golang  
  `1.17.4 +`
* Apache Kafka (confluentinc)  
  `5.4.6 +`

* Liblaries  
 `mux` install with `go get -u github.com/gorilla/mux`  
 `mongo driver` install with `go get go.mongodb.org/mongo-driver/mongo`  
 `bson` import by `gopkg.in/mgo.v2/bson`  

Package **mux** implements a request router and dispatcher for matching incoming requests to handler.  
Package **mongo** provides a MongoDB Driver API for Go.  
Package **bson** is an implementation of the BSON specification for Go.  

## Setup Database

MongoDB is launched from docker-compose.  
Create database `goRestApi`    

## Application Configuration  
  
Application configuration is contained in the `.env` file.  
Function responsible for configuration processing can be found in `src/config/config.go`  
It allows you to get configuration using specific key `get := config.GetEnvByKey`  
Then you can indicate which specific key you are interested in, ex. `get("DB_HOST")`  

## Install Kafka-Go on Apple M1

Below I presented configuration for the M1 processor, you can also follow these steps for the x86 architecture, omitting `arch -arm64`.  
For local development add `127.0.0.1	kafka` to your `/etc/hosts` change IP if needed.  
Install **librdkafka** `src % arch -arm64 brew install librdkafka` -> Apple M1 platform.  
Install other libraries `brew install openssl zstd pkg-config`  
Export openssl path `export PKG_CONFIG_PATH="/opt/homebrew/opt/openssl@3/lib/pkgconfig"`  
`kafka` install with `go get -tags dynamic gopkg.in/confluentinc/confluent-kafka-go.v1/kafka`  
Change your path for zookeeper docker volumes in docker-compose file.  
For test environment use included docker-compose and initialize new kafka cluster from kafka manager gui.  
Compile or run with `go run -tags dynamic main.go`  

## REST API  

This small project implement simple REST API based on Golang, MongoDB and Apache Kafka.  
Information about bitcoin rates is downloaded every 3 seconds and send to kafka topic `bitcoin_price`, payload is waiting for the consumer.  
To automatically retrieve BT rates information, run application with `-- tickBtPrice` parameter.  
Consumer consume message and save it to mongo database.  

# Table of endpoint  

|Method    |Endpoint            |Note                                                                                 |
|----------|--------------------|-------------------------------------------------------------------------------------|
|GET			 |`/`                 |Get service status, save response in mongo collection with requestor IP address      |
|GET       |`/service_statuses` |Get serice status collection                                                         |
|GET       |`/bitcoin_price`    |Get bitcoin price from Bitcoin Price Index (USD) and openexchangerates.org (GBP, EUR)|
|GET       |`/bitcoin_prices`   |Get bitcoin price collection                                                         |
|PUT       |`/bitcoin_price?object_id=61ce406bf795308e4a66fc03` |Update bitcoin price document                        |
|POST      |`/bitcoin_price`    |Insert bitcoin price to mongo database                                               |

# Sequence Diagram

![GoRestApiUML](https://syngress.pl/images/go_rest_api/go_rest_api_diagram.png)  

**THIS PROJECT IS STILL UNDER CONSTRUCTION**  
