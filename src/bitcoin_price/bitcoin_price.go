package bitcoin_price

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	domain "go_rest_api/domain/entity"
	"go_rest_api/domain/repository"
	"go_rest_api/helper/formatted_time"
	"go_rest_api/kafka/producer"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UpdateMessage struct {
	Message string `json:"message"`
}

func GetBitcoinPrice(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	data := processBtData()

	var payload = domain.BitcoinPricePayload()

	json.Unmarshal([]byte(string(data)), &payload)

	fmt.Println(json.NewEncoder(w).Encode(payload))
}

func processBtData() (btPricePayload []byte) {
	response, err := bitcoinPriceCall()
	if err != nil {
		log.Fatal(err)
	}

	defer response.Close()

	_, formattedTime := formatted_time.GetFormattedTime()
	fmt.Println(formattedTime, ": hit get bitcoin price")

	responseData, err := ioutil.ReadAll(response)
	if err != nil {
		log.Fatal(err)
	}

	var payload = domain.BitcoinPricePayload()

	json.Unmarshal([]byte(string(responseData)), &payload)

	producer.SendMessage(responseData)

	return responseData
}

func bitcoinPriceCall() (io.ReadCloser, error) {
	response, err := http.Get("https://api.coindesk.com/v1/bpi/currentprice.json")

	return response.Body, err
}

func HeartBeatBtPrice(params string) {
	if params == "tickBtPrice" {
		seconds := 3
		for range time.Tick(time.Duration(seconds) * time.Second) {
			processBtData()
		}
	}
}

func PostBitcoinPrice(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	_, formattedTime := formatted_time.GetFormattedTime()
	fmt.Println(formattedTime, ": hit post bitcoin price")

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(string(err.Error()))
	}

	var payload = domain.BitcoinPricePayload()

	json.Unmarshal(body, &payload)

	repository.InsertOne(body, "bitcoin_price")

	var data bytes.Buffer
	rsp := io.MultiWriter(w, &data)
	json.NewDecoder(r.Body).Decode(&payload)

	fmt.Println(json.NewEncoder(rsp).Encode(payload))
}

func UpdateBitcoinPrice(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id, _ := primitive.ObjectIDFromHex(r.URL.Query().Get("object_id"))
	_, formattedTime := formatted_time.GetFormattedTime()
	fmt.Println(formattedTime, ": hit update bitcoin price")

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(string(err.Error()))
	}

	repository.UpdateOne(id, body, "bitcoin_price")

	message := UpdateMessage{"updated succesfully"}

	fmt.Println(json.NewEncoder(w).Encode(message))
}

func BitcoinPriceCollection(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	_, formattedTime := formatted_time.GetFormattedTime()

	fmt.Println(formattedTime, ": hit for bitcoin price collection")

	bitcoinPrices, err := repository.GetCollection("bitcoin_price")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(json.NewEncoder(w).Encode(bitcoinPrices))
}
