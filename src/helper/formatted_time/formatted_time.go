package formatted_time

import (
	"fmt"
	"time"
)

func GetFormattedTime() (time.Time, string) {
	timeNow := time.Now()
	formattedTime := fmt.Sprintf("%d-%02d-%02d - %02d:%02d:%02d",
		timeNow.Year(), timeNow.Month(), timeNow.Day(),
		timeNow.Hour(), timeNow.Minute(), timeNow.Second())

	return timeNow, formattedTime
}
