package service_status

import (
	"encoding/json"
	"fmt"
	"go_rest_api/domain/repository"
	"go_rest_api/helper/formatted_time"
	"log"
	"net/http"
	"time"
)

type Status struct {
	Status string    `json:"service_status"`
	Date   time.Time `json:"date"`
	Ip     string    `json:"requestor_ip"`
}

var serviceStatus Status

func GetServiceStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	timeNow, formattedTime := formatted_time.GetFormattedTime()

	fmt.Println(formattedTime, ": hit service status document")

	serviceStatus = Status{
		Status: "active",
		Date:   timeNow,
		Ip:     getIP(r),
	}

	marshalledPayload, err := json.Marshal(Status(serviceStatus))

	if err != nil {
		log.Fatal(err)
	}

	repository.InsertOne(marshalledPayload, "service_status")

	fmt.Println(json.NewEncoder(w).Encode(serviceStatus))
}

func ServiceStatusCollection(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	_, formattedTime := formatted_time.GetFormattedTime()

	fmt.Println(formattedTime, ": hit for service status collection")

	serviceStatuses, err := repository.GetCollection("service_status")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(json.NewEncoder(w).Encode(serviceStatuses))
}

func getIP(r *http.Request) string {
	forwarded := r.Header.Get("X-FORWARDED-FOR")
	if forwarded != "" {
		return forwarded
	}
	return r.RemoteAddr
}
