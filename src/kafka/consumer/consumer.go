package consumer

import (
	"encoding/json"
	"fmt"
	"go_rest_api/config"
	"go_rest_api/domain/repository"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func ConsumeMessage() {

	get := config.GetEnvByKey
	host := get("KAFKA_BROKER_HOST")
	port := get("KAFKA_BROKER_PORT")
	id := get("KAFKA_GROUP_ID")

	broker := "%s:%s"
	group := "%s"
	topics := []string{"bitcoin_price"}
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":     fmt.Sprintf(broker, host, port),
		"broker.address.family": "v4",
		"group.id":              fmt.Sprintf(group, id),
		"session.timeout.ms":    6000,
		"auto.offset.reset":     "earliest"})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create consumer: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Created Consumer %v\n", c)

	err = c.SubscribeTopics(topics, nil)
	if err != nil {
		fmt.Println(string(err.Error()))
	}

	run := true

	for run {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(100)
			if ev == nil {
				continue
			}

			switch e := ev.(type) {
			case *kafka.Message:
				fmt.Printf("%% Message on %s:\n%s\n",
					e.TopicPartition, string(e.Value))
				if e.Headers != nil {
					fmt.Printf("%% Headers: %v\n", e.Headers)
				}
				saveMessage(e.Value)
			case kafka.Error:
				fmt.Fprintf(os.Stderr, "%% Error: %v: %v\n", e.Code(), e)
				if e.Code() == kafka.ErrAllBrokersDown {
					run = false
				}
			default:
				fmt.Printf("Ignored %v\n", e)
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	c.Close()
}

func saveMessage(messageBody []byte) {
	var result map[string]interface{}
	json.Unmarshal([]byte(messageBody), &result)
	delete(result, "time")
	delete(result, "disclaimer")
	delete(result, "chartName")
	data, err := json.Marshal(result)
	if err != nil {
		log.Fatal(err)
	}

	repository.InsertOne(data, "bitcoin_price")
}
