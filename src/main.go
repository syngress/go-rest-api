package main

import (
	"go_rest_api/bitcoin_price"
	"go_rest_api/config"
	"go_rest_api/kafka/consumer"
	"go_rest_api/service_status"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func routing() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", service_status.GetServiceStatus).Methods("GET")
	myRouter.HandleFunc("/service_statuses", service_status.ServiceStatusCollection).Methods("GET")
	myRouter.HandleFunc("/bitcoin_price", bitcoin_price.GetBitcoinPrice).Methods("GET")
	myRouter.HandleFunc("/bitcoin_prices", bitcoin_price.BitcoinPriceCollection).Methods("GET")
	myRouter.HandleFunc("/bitcoin_price", bitcoin_price.PostBitcoinPrice).Methods("POST")
	myRouter.HandleFunc("/bitcoin_price", bitcoin_price.UpdateBitcoinPrice).Methods("PUT")
	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func param() (osArgs string) {
	if len(os.Args) > 1 {
		return string(os.Args[2])
	}

	return ""
}

func main() {
	config.LoadEnv()
	go consumer.ConsumeMessage()
	go bitcoin_price.HeartBeatBtPrice(param())
	routing()
}
