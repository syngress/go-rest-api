package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

func GetEnvByKey(key string) string {
	return os.Getenv(key)
}

func LoadEnv() {
	err := godotenv.Load("../.env")
	if err != nil {
		log.Fatalf("Error loadig .env file")
		os.Exit(1)
	}
}
