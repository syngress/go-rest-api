package domain

type CurrencyData struct {
	Code        string  `json:"code"`
	Symbol      string  `json:"symbol"`
	Rate        string  `json:"rate"`
	Description string  `json:"description"`
	RateFloat   float32 `json:"rate_float"`
}

type Currency struct {
	USD CurrencyData `json:"USD"`
	GBP CurrencyData `json:"GBP"`
	EUR CurrencyData `json:"EUR"`
}

type Payload struct {
	Key Currency `json:"bpi"`
}

var bitcoinPricePayload Payload

func BitcoinPricePayload() Payload {
	return bitcoinPricePayload
}
