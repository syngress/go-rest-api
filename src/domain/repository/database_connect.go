package repository

import (
	"context"
	"fmt"
	"go_rest_api/config"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func connect() (*mongo.Client, error) {
	get := config.GetEnvByKey

	host := get("DB_HOST")
	port := get("DB_PORT")
	database_connection := ("mongodb://%s:%s")

	clientOptions := options.Client().ApplyURI(fmt.Sprintf(database_connection, host, port))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	return client, err
}
