package repository

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/mgo.v2/bson"
)

func InsertOne(payload []byte, dbcollection string) {
	client, err := connect()
	if err != nil {
		log.Fatal(err)
	}

	defer client.Disconnect(context.TODO())

	collection := client.Database("goRestApi").Collection(dbcollection)

	var jsonData interface{}
	err = bson.UnmarshalJSON([]byte(payload), &jsonData)
	if err != nil {
		panic(err)
	}

	insertResult, err := collection.InsertOne(context.TODO(), jsonData)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted a Single Document to MongoDB: ", insertResult.InsertedID)
}

func UpdateOne(objectID primitive.ObjectID, payload []byte, dbcollection string) {
	client, err := connect()
	if err != nil {
		log.Fatal(err)
	}

	defer client.Disconnect(context.TODO())

	collection := client.Database("goRestApi").Collection(dbcollection)

	var jsonData interface{}
	err = bson.UnmarshalJSON([]byte(payload), &jsonData)
	if err != nil {
		panic(err)
	}

	filter := bson.M{"_id": bson.M{"$eq": objectID}}
	update := bson.M{"$set": jsonData}

	collection.UpdateOne(
		context.TODO(),
		filter,
		update,
	)
	if err != nil {
		log.Fatal(err)
	}

}

func GetCollection(dbcollection string) ([]bson.M, error) {
	client, err := connect()
	if err != nil {
		log.Fatal(err)
	}

	defer client.Disconnect(context.TODO())

	collection := client.Database("goRestApi").Collection(dbcollection)
	cursor, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	var collectionData []bson.M
	if err = cursor.All(context.TODO(), &collectionData); err != nil {
		log.Fatal(err)
	}

	fmt.Println("Get collection from MongoDB")

	return collectionData, err
}
